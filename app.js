document.addEventListener('keydown', function(e) {
  if (e.keyCode !== 0) {
    document.querySelector('.keycode_display').style.display = 'block';
    document.querySelector('.main_area').style.display = 'none';
    document.querySelector('.keycode').textContent = e.keyCode;
    document.querySelector('.code').textContent = e.code;
    document.querySelector('.key').textContent = e.key;
    document.querySelector('.location').textContent = e.location;
  }
});